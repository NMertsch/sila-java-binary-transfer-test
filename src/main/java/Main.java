import sila_java.library.server_base.binary_transfer.database.impl.H2BinaryDatabase;
import sila_java.library.server_base.binary_transfer.upload.UploadService;

import java.sql.SQLException;
import java.util.UUID;

public class Main {
    public static void main(String[] args) throws SQLException {
        try (H2BinaryDatabase binaryDatabase = new H2BinaryDatabase(UUID.randomUUID())) {
            UploadService uploadService = new UploadService(binaryDatabase);
            System.out.println(uploadService.getClass().getName());
        }
    }
}
